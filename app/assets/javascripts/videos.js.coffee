$ ->
  # dirty workaround for: "firefox seems unable to playback"
  captureUserMedia = (callback) ->
    navigator.getUserMedia = navigator.mozGetUserMedia or navigator.webkitGetUserMedia
    navigator.getUserMedia
      audio: true
      video: true
    , ((stream) ->
      video.src = URL.createObjectURL(stream)
      video.muted = true
      video.controls = true
      audio.muted = true
      video.play()
      callback stream
      return
    ), (error) ->
      console.error error
      return

    return

  video           = document.getElementById("video")
  audio           = document.getElementById("audio")
  downloadURL     = document.getElementById("download-url")
  startRecording  = document.getElementById("start-recording")
  stopRecording   = document.getElementById("stop-recording")
  uploadRecording = document.getElementById("upload-recording")

  startRecording.onclick = ->
    startRecording.disabled = true
    stopRecording.disabled = false
    uploadRecording.disabled = true
    captureUserMedia (stream) ->
      window.audioRecorder = window.RecordRTC(stream, 
        type: "audio"
        bufferSize: 16384
      )
      window.videoRecorder = window.RecordRTC(stream,
        type: "video"
        video: {
          width: 640,
          height: 480
        }
        canvas: {
          width: 640,
          height: 480
        }
      )
      window.videoRecorder.startRecording()
      window.audioRecorder.startRecording()
      return

    return

  stopRecording.onclick = ->
    stopRecording.disabled = true
    startRecording.disabled = false
    uploadRecording.disabled = false
    window.videoRecorder.stopRecording()
    window.audioRecorder.stopRecording()

    return

  uploadRecording.onclick = ->
    token = $('meta[name="csrf-token"]').attr('content');
    formData = new FormData()
    videoBlob = window.videoRecorder.getBlob()
    audioBlob = window.audioRecorder.getBlob()
    formData.append("_video", videoBlob)
    formData.append("_audio", audioBlob)
    formData.append("video[title]", $('#video_title').val())
    formData.append("user_agent", navigator.userAgent)
    request = new XMLHttpRequest()

    request.onreadystatechange = ->
      if request.readyState == 4 && request.status == 200
        window.location.href = request.responseURL

    request.open('POST', "/videos")
    request.setRequestHeader('X-CSRF-Token', token)
    request.send(formData)


