class Video < ActiveRecord::Base
  default_scope -> {order("id desc")}

  validates :title, presence: true

  #mount_uploader :file, FileUploader

  def file_mp4
    "/video-files/#{file}.mp4"
  end

  def file_webm
    "/video-files/#{file}.webm"
  end

  def file_ogg
    "/video-files/#{file}.ogv"
  end

  def files
    [
      #file_ogv,
      file_mp4,
      file_webm
    ]
  end

end