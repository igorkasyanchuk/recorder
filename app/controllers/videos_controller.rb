class VideosController < ApplicationController
  before_action :set_video, only: [:show, :edit, :update, :destroy]

  # GET /videos
  # GET /videos.json
  def index
    @videos = Video.all
  end

  # GET /videos/1
  # GET /videos/1.json
  def show
  end

  # GET /videos/new
  def new
    @video = Video.new
  end

  # GET /videos/1/edit
  def edit
  end

  def process_video_chrome(uuid)
    # run_command("avconv -i uploads/#{uuid}.webm -ss 00:00:00 -vcodec libx264 -an public/video-files/#{uuid}-no-audio.mp4", "webm => mp4-no-audio")
    # run_command("avconv -i public/video-files/#{uuid}-no-audio.mp4 -i uploads/#{uuid}.wav -ss 00:00:00 -acodec aac -strict experimental public/video-files/#{uuid}.mp4", "mp4-no-audio + wav => mp4")
    # run_command("avconv -i public/video-files/#{uuid}.mp4 -vcodec libvpx public/video-files/#{uuid}.webm", "mp4 => webm")
    run_command("avconv -i uploads/#{uuid}.webm -ss 00:00:00 -i uploads/#{uuid}.wav -ss 00:00:00 -acodec aac -strict experimental public/video-files/#{uuid}.mp4", "webm + wav => mp4")
    run_command("avconv -i uploads/#{uuid}.webm -ss 00:00:01 -i uploads/#{uuid}.wav -ss 00:00:00 public/video-files/#{uuid}.webm", "webm + wav => webm")
  end

  def process_video_firefox(uuid)
    run_command("avconv -i uploads/#{uuid}.webm -vn public/video-files/#{uuid}.ogg", "webm => ogg")
    run_command("avconv -i uploads/#{uuid}.webm -ss 00:00:00 -vcodec libx264 -an public/video-files/#{uuid}-no-audio.mp4", "webm => mp4-no-audio")
    run_command("avconv -i public/video-files/#{uuid}-no-audio.mp4 -i public/video-files/#{uuid}.ogg -ss 00:00:00 -acodec aac -strict experimental public/video-files/#{uuid}.mp4", "mp4+ogg => mp4")
    run_command("cp uploads/#{uuid}.webm public/video-files/#{uuid}.webm", "move")
  end

  def create
    logger.info "\n"
    logger.info "User Agent: #{params[:user_agent]}".yellow
    logger.info "Audio: #{params['_audio'].tempfile.size}".yellow
    logger.info "Video: #{params['_video'].tempfile.size}".yellow
    logger.info "------"

    uuid = UUID.generate
    File.open("uploads/#{uuid}.wav", "wb") do |f|
      f.write(params['_audio'].tempfile.read)
    end

    File.open("uploads/#{uuid}.webm", "wb") do |f|
      f.write(params['_video'].tempfile.read)
    end
    
    if params[:user_agent].include?("Chrome")
      process_video_chrome(uuid)
    else
      process_video_firefox(uuid)
    end

    @video = Video.new(video_params)

    @video.file = uuid

    respond_to do |format|
      if @video.save
        format.html { redirect_to @video, notice: 'Video was successfully created.' }
        format.json { render action: 'show', status: :created, location: @video }
      else
        format.html { render action: 'new' }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /videos/1
  # PATCH/PUT /videos/1.json
  def update
    respond_to do |format|
      if @video.update(video_params)
        format.html { redirect_to @video, notice: 'Video was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @video.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /videos/1
  # DELETE /videos/1.json
  def destroy
    @video.destroy
    respond_to do |format|
      format.html { redirect_to videos_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_video
      @video = Video.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def video_params
      params.require(:video).permit(:title, :file)
    end

    def run_command(command, tag)
      logger.info "now :: #{tag}".green
      logger.info command.pur
      system(command)
      logger.info "done :: #{tag}".green
    end
end
