# encoding: utf-8

class FileUploader < CarrierWave::Uploader::Base
  include CarrierWave::Video

  #after :store, :convert_video

  # version :mp4 do
  #   process encode_video: [:mp4]
  # end

  # version :webm do
  #   process encode_video: [:webm]
  # end

  storage :file

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # def filename
  #   "#{original_filename}.webm" if original_filename.present?
  # end

  # def convert_video(args)
  #   # libx264
  #   #command = "avconv -i #{path} -vcodec libx264 -y #{path}.mp4"
  #   command = "avconv -i #{path} -vcodec libx264 -pix_fmt yuv420p -preset slower -crf 18 -y #{path}.mp4"
  #   #command = "avconv -i #{path} -vcodec mpeg4 -acodec aac -strict experimental -y #{path}.mp4"
  #   #command = "./video.sh #{path} #{path}"
  #   Rails.logger.info "---"
  #   Rails.logger.info command
  #   Rails.logger.info "---"
  #   system(command)
  #   args
  # end

end
